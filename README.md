xz reading C++ demo
===================

liblzma taken from project [XZ Utils](https://tukaani.org/xz/) version 5.2.5

bxzstr C++ wrapper taken from (https://github.com/tmaklin/bxzstr)

There are two caveats:

* define `LZMA_API_STATIC` to link against static liblzma
* open input stream as `std::ios_base::binary`! (at least, in Windows)

